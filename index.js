// console.log("Hello World");


// // [SECTION] Functions
// // Functions in javascript are lines/blocks of codes that tell our devices to perform a certain task when called/invoked.
// // Theyare also used to prevent repeating lines/block of codes that perform the same task function.

// // Function Declaration
// 	// funciton statement defines a funciton with parameters

// 	// function keyword - used to define a javascript funciton
// 	// function name - is used so we are able to call/invoke a declared function
// 	// function code block ({}) - the statement which comprise the body of the function. Theis is where the code to be executed is in.

// // function declaration // function name
// // funcitgon name requires open and close parenthesis beside it
// function printName() { //code block
// 	console.log("My name is John"); //function statement
// }; //delimeter

// printName(); //Funciton invocation

// // [HOISTING]
// // Hoisting is Javascript behavior for certain varuables and functions to run or use before their declaration
// declaredFunction();
// // make sure the funciton is existing whenever we call/invoke a function

// function declaredFunction() {
// 		console.log("Hello World");
// }

// declaredFunction();

// // [Function Expression] 
// // A function can be also stored in a variable. That is called a function expression
// // Hositing is allowed in function declarations, however, we could not hoist through function expressions.

// // variableFunction("Hello again 2");

// let variableFunction = function(message) {
// 	console.log(message);
// };

// variableFunction("Hello again 2");

// let funcExpression = function funcName() {
// 	console.log("Hello from the other side");
// };

// funcExpression();
// // funcName();
// // Error: Whenever we are calling a named function stored in a variable, we just call the variable name, not the function name.

// console.log("-----------");
// console.log("[Reassigning Funcitons]");

// declaredFunction();

// declaredFunction = function() {
// 	console.log("updated declaredFunction")
// };

// declaredFunction();


// funcExpression();

// funcExpression = function() {
// 	console.log("updated funcExpressxion");
// };

// funcExpression();

// // Constant Funciton
// const constantFunction = function() {
// 	console.log("Initialized with const.")
// };

// constantFunction();

// /*constantFunction = function() {
// 	console.log("New value");
// };
// */ // function with const keyword cannot be reassigned.
// // Scope is the accessiblity/visibility of a variable in the code

// /*
// 	Javascritp variables has 3 types of scope:
// 	1. local / block scope
// 	2. global scope
// 	3. function scope
// */
// console.log("-----------");
// console.log("[Function Scoping]");

// {
// 	let localVar = "Armando Perez";
// 	console.log(localVar);
// }

// // console.log(localVar);

// let globalVar = "Mr. Worldwide";
// console.log(globalVar);

// // Variables/functions declared inside code blocks are considred local variables land cannot be accessed/invoked outside their scope

// {
// 	console.log(globalVar);
// }

// // [Funciton Scoping]
// // Varaibles defined inside a function are not accessible/visible outside the function.
// // Variables declared with var, let, and const are quite similar when declared inside a funciton.
// function shownames() {
// 	var functionVar = "Joe";
// 	const functionConst = "John";
// 	let functionLet = "Jane";

// 	console.log(functionVar);
// 	console.log(functionConst);
// 	console.log(functionLet);
// };

// shownames();

// // Error - These are funciton scoped variables and cannot be accessed outside the funciton they were declared in.
// 	// console.log(functionVar);
// 	// console.log(functionConst);
// 	// console.log(functionLet);


// // Nested functions 
// 	// You can create another funciton inside a function, called Nested Function.


// function myNewFunction() {
// 	let name = "Jane";

// 	function nestedFunction() {
// 		let nestedName = "John";
// 		console.log(name);
// 	}

// 	nestedFunction(); // result not defined as error.
// 	console.log(nestedFunction);
// };


// myNewFunction();

// let globalName = 'Zuitt';

// function myNewFunction2() {
// 	let nameInside = "Renz";
// 	console.log(globalName);
// };

// myNewFunction2();

// // alert() allows us to show a small window at the top of our browser page to show information to our users.
// // alert("Sample Alrt");

// // alert("You cannot access this page.")

// function showSampleAlert() {
// 	alert("Hello, user");
// };


// showSampleAlert();

// // alert messages inside a function will only execute whenever we call or invoke the function

// console.log("I will only log in the console when the alert is dismissed");

// // Notes on the use of alert();
// 	// Show only an alert for short dialogs/messages to the user
// 	// Do not overuse alert() because the program/js has to wait for it to be dismissed before continuing.

// // [Prompt]
// 	// prompt() allow us to show small window at the top of the browser to gather user input.

// 	// let samplePrompt = prompt("Enter your Full Name");

// 	// prompt is invoked when it is declared even when it's assigned to a variable

// 	// Everything inputted in prompts are converted to string first before being stored in a variable if any.
// 	// console.log(samplePrompt);

// 	// console.log(typeof samplePrompt);

// 	// console.log("Hello, " + samplePrompt);

// 	function printWelcomeMessage() {
// 		let firstName = prompt("Enter your first name: ");
// 		let lastName = prompt("Enter your last name");

// 		console.log("Hello, " + firstName + " " + lastName);
// 		console.log("Welcome to my page!");
// 	}


// 	printWelcomeMessage();

// 	function getCourses() {
// 		let courses = ["Science 101", "Math 101", "English 101"];
// 		console.log(courses);
// 	}

// 	getCourses();

// 	// [SECTION] Function Naming Conventions

// 	// Funciton name should be definitive of the task it will perform.

// 	function getCourses() {
// 		let courses = ["Science 101", "Math 101", "English 101"];
// 		console.log(courses);
// 	}

// 	getCourses();

// 	// Avoid generic names to avoid confusion within our code.

// 	function get() {
// 		let name = "Jamie";
// 		console.log(name);
// 	}

// 	get();

// 	// Avoid pointless and inappropriate function names, example: foo, bar, etc.

// 	function foo() {
// 		console.log(25%5);
// 	}

// 	foo();



// Name your function in small caps. Follow camelCase when naming varialbes and functions

function displayCarInfo() {
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
}

displayCarInfo();




